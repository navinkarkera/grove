# Create service account for the periodic build notifier to get an access token, that is
# used to retrieve logs from jobs.

apiVersion: v1
kind: ServiceAccount
metadata:
  name: periodic-build-notifier
  namespace: openfaas-fn
secrets:
  - name: periodic-build-notifier-token

---

# Create an additional secret which binds to the service account. Kubernetes is not
# allowing to create default secrets -- like the one generated upon service account
# creation -- with specific names, so we need this token duplication workaround. Both
# secrets will live in the cluster, and both will be automatically updated if needed.

apiVersion: v1
kind: Secret
metadata:
  name: periodic-build-notifier-token
  namespace: openfaas-fn
  annotations:
    kubernetes.io/service-account.name: periodic-build-notifier
type: kubernetes.io/service-account-token

---

# Create a role to allow listing of pods and retrieving their logs in all namespaces,
# hence we use ClusterRole.

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: periodic-build-notifier
rules:
  - apiGroups: [""]  # "" indicates the core API group
    resources: ["pods", "pods/log"]
    verbs: ["get", "list"]

---

# Bind the cluster role defined above to the cluster, allowing access to all namespaces.

apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: periodic-build-notifier-pod-reader-global
subjects:
  - kind: ServiceAccount
    name: periodic-build-notifier
    namespace: openfaas-fn
roleRef:
  kind: ClusterRole
  name: periodic-build-notifier
  apiGroup: rbac.authorization.k8s.io
