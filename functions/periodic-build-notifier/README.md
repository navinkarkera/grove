# Periodic build notifier

This function receives GitLab Webhooks of GitLab Pipeline statuses.

When a pipeline created by Gitlab Schedules fails, the log file is downloaded for the failed job, compressed and emailed to the given recipients. Emails are not generated for pipelines triggered manually or by a merge request.

## Prerequisites

1. Working Grove cluster
2. OpenFAAS CLI installed [^1]
3. SMTP configuration instructions and credentials

[^1] Although Grove is containerizing tools used to manage it, OpenFAAS is treated as an extension of Grove, not a necessary functional component. Therefore, OpenFAAS CLI should be installed by the user, locally.

## Installation

1. Make sure you are set up to work with Grove locally (see [Working Locally](/docs/user-guides/working-locally.md)).
2. Change into the `control` directory, e.g. `cd my-cluster/control/`.
3. Create a service account for the periodic builder which will allow access to Kubernetes resources for the sake of retrieving build logs from jobs.

   ```shell
   # Create the service account -- the file path is intentionally starting with /workspace
   ./kubectl apply -f /workspace/grove/functions/periodic-build-notifier/resources.yml
   ```

4. Run the following command to create the email sender Kubernetes secret, and replace `<SECRET>` with your SMTP password.

   ```shell
   ./kubectl --namespace openfaas-fn create secret generic email-sender-password \
      --from-literal email-sender-password="<SECRET>"
   ```

5. Run the following command to create the GitLab token used to fetch pipeline information and job logs, and replace `<SECRET>` with your password.

   ```shell
   ./kubectl --namespace openfaas-fn create secret generic gitlab-token \
      --from-literal gitlab-token="<SECRET>"
   ```

6. Get the OpenFAAS admin password by executing `./tf output -json openfaas_ingress_password`.
7. Change into the `grove/functions` library, e.g. `cd my-cluster/grove/functions/`
8. Login to the Grove hosted OpenFAAS and GitLab container registry

   ```shell
   export OPENFAAS_URL=https://openfaas.<YOUR_CLUSTER_HOSTNAME>
   faas-cli login --tls-no-verify -g $OPENFAAS_URL -u admin -p "<OPENFAAS_ADMIN_PASSWORD>"
   docker login <GITLAB_CI_REGISTRY> --username `<GITLAB_USERNAME>` --password `<GITLAB_PASSWORD>`
   ```

9. Build and deploy the function as shown below. Note that if you did not override the function in any way, you don't need to build and push the image as that image is managed by OpenCraft.

   ```shell
   faas-cli build --no-cache -f periodic-build-notifier.yml
   faas-cli push -f periodic-build-notifier.yml
   faas-cli deploy \
      -f periodic-build-notifier.yml \
      --tls-no-verify \
      --gateway "${OPENFAAS_URL}" \
      --env EMAIL_SENDER="<YOUR_SENDER_ADDRESS>" \
      --env EMAIL_RECIPIENT="<YOUR_RECIPIENT_ADDRESS>" \
      --env EMAIL_SMTP_HOST="<YOUR_SMTP_HOST>" \
      --env EMAIL_SMTP_PORT="<YOUR_SMTP_PORT>" \
      --env EMAIL_SMTP_USER="<YOUR_SMTP_USER>"
   ```

Now, the function is ready to use. You can call the function by using the basic auth credentials used for logging in to OpenFAAS UI by providing it in the URL. Example: `https://admin:<OPENFAAS_ADMIN_PASSWORD>@openfaas.<YOUR_CLUSTER_URL>/function/periodic-build-notifier`.

From this point the function can be set as a webhook receiver address for pipelines in GitLab.
