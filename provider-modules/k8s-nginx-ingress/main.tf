# install nginx ingress controller
resource "helm_release" "nginx_ingress" {
  name       = "ingress-nginx"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart      = "ingress-nginx"
  version    = "4.1.0"
  namespace  = var.ingress_namespace

  set {
    name  = "rbac.create"
    value = true
  }
}

# install cert manager
resource "helm_release" "ingress_cert_manager" {
  name       = "cert-manager"
  repository = "https://charts.jetstack.io"
  chart      = "cert-manager"
  version    = "v1.8.0"
  namespace  = var.ingress_namespace
  values = [
    file("${path.module}/cert_manager.yml")
  ]
  depends_on = [helm_release.nginx_ingress]
}
