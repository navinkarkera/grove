# k8s-nginx-ingress

This module installs [NGINX Ingress Controller](https://kubernetes.github.io/ingress-nginx/) and [cert-manager](https://cert-manager.io/docs/) using Helm.

# Variables

- `ingress_namespace`: The namespace under which those resources will be installed.

# Output

This module doesn't provide any output.
