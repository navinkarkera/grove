resource "random_password" "user_passwords" {
  for_each         = toset(var.tutor_instances)
  length           = 16
  special          = true
  override_special = "@$%&()-_=+[]{}<>:?"
}

resource "mongodbatlas_database_user" "users" {
  for_each           = toset(var.tutor_instances)
  project_id         = var.mongodbatlas_project_id
  auth_database_name = "admin"

  username = each.key
  password = random_password.user_passwords[each.key].result

  roles {
    role_name     = "readAnyDatabase"
    database_name = "admin"
  }

  roles {
    role_name     = "readWrite"
    database_name = each.key
  }

  roles {
    role_name     = "readWrite"
    database_name = "${each.key}-cs_comments_service"
  }

  scopes {
    type = "CLUSTER"
    name = mongodbatlas_cluster.cluster.name
  }
}
