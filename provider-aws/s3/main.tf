################################################################################
# Buckets
################################################################################
resource "aws_s3_bucket" "s3_bucket" {
  # Let us use the bucket prefix instead of bucket, so that Terraform can create
  # a identifiable unique name
  bucket_prefix = substr("${var.bucket_prefix}-", 0, 37)
  tags          = var.tags
}

resource "aws_kms_key" "s3_key" {
  description             = "S3 encryption key for ${aws_s3_bucket.s3_bucket.id} bucket"
  deletion_window_in_days = 10
}

resource "aws_s3_bucket_cors_configuration" "s3_cors" {
  bucket = aws_s3_bucket.s3_bucket.id

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST", "GET"]
    allowed_origins = ["*"]
  }
}

resource "aws_s3_bucket_acl" "s3_acl" {
  bucket = aws_s3_bucket.s3_bucket.id
  acl    = "private"
}

resource "aws_s3_bucket_server_side_encryption_configuration" "s3_encryption" {
  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.s3_key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}

# Versioning protects us from accidental file deletion and override. Lifecycle rules clean up old file versions.
resource "aws_s3_bucket_versioning" "s3_versioning" {
  bucket = aws_s3_bucket.s3_bucket.id

  versioning_configuration {
    status = "Enabled"
  }
}

resource "aws_s3_bucket_lifecycle_configuration" "s3_lifecycle" {
  bucket = aws_s3_bucket.s3_bucket.id

  rule {
    id     = "tutor-env-rule-1"
    status = "Enabled"

    expiration {
      # Delete files that have no available versions.
      expired_object_delete_marker = true
    }

    noncurrent_version_expiration {
      noncurrent_days = 30
    }
  }
}

################################################################################
# edX app IAM
################################################################################
data "aws_iam_policy_document" "s3_iam_policy_document" {
  version = "2012-10-17"
  statement {
    actions = [
      "s3:*",
    ]

    resources = [
      aws_s3_bucket.s3_bucket.arn,
      "${aws_s3_bucket.s3_bucket.arn}/*",
    ]
  }
  statement {
    actions = [
      "kms:*",
    ]

    resources = [
      aws_kms_key.s3_key.arn,
    ]
  }
}

resource "aws_iam_policy" "s3_iam_policy" {
  name        = lower(join("-", [var.bucket_prefix, "s3-full-access"]))
  description = "Grants full access to s3 resources in the ${aws_s3_bucket.s3_bucket.id} bucket"

  policy = data.aws_iam_policy_document.s3_iam_policy_document.json
}

resource "aws_iam_user" "s3_user" {
  name = lower(join("-", [var.bucket_prefix, "s3", "iam"]))
}

resource "aws_iam_user_policy_attachment" "s3_policy_attachment" {
  user       = aws_iam_user.s3_user.name
  policy_arn = aws_iam_policy.s3_iam_policy.arn
}

resource "aws_iam_access_key" "s3_access_key" {
  user = aws_iam_user.s3_user.name
}

