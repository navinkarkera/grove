variable "cluster_name" { type = string }
variable "cluster_max_node_count" { default = 5 } # Sets the maximum size of the EKS autoscaling group (asg_max_size). High values = $$$$
variable "aws_region" { default = "us-east-1" }

variable "container_registry_server" { default = "registry.gitlab.com" }
variable "dependency_proxy_server" { default = "gitlab.com" }
variable "gitlab_group_deploy_token_username" { type = string }
variable "gitlab_group_deploy_token_password" { type = string }
variable "gitlab_cluster_agent_token" {
  type        = string
  description = "Token retrieved for Gitlab cluster agent"
}

variable "tutor_instances" { type = list(string) }

variable "rds_instance_class" { default = "db.t3.micro" }
variable "rds_mysql_version" { default = "5.7" }
variable "rds_min_storage" { default = 10 }
variable "rds_max_storage" { default = 15 }

variable "rds_backup_retention_period" { default = 35 }

variable "rds_storage_encrypted" { default = true }

variable "alert_manager_config" {
  type        = string
  description = "Alert Manager configuration as a YAML-encoded string"
  default     = "{}"
  validation {
    condition     = can(yamldecode(var.alert_manager_config))
    error_message = "The alert_manager_config value must be a valid YAML-encoded string."
  }
}

variable "enable_monitoring_ingress" {
  type        = bool
  default     = false
  description = "Whether to enable ingress for monitoring services."
}

variable "enable_openfaas" {
  type        = bool
  default     = false
  description = "Whether to enable OpenFAAS."
}

variable "cluster_domain" {
  type        = string
  default     = "grove.local"
  description = "Domain used as the base for monitoring services."
}

variable "lets_encrypt_notification_inbox" {
  type        = string
  default     = "contact@example.com"
  description = "Email to send any email notifications about Letsencrypt"
}
