variable "cluster_name" { type = string }
variable "cluster_max_node_count" { default = 5 }

variable "do_token" { type = string }
variable "do_region" { default = "sfo3" }

variable "mysql_cluster_instance_size" {
  type    = string
  default = "db-s-1vcpu-1gb"
}
variable "mysql_cluster_node_count" {
  type    = number
  default = 1
}
variable "mysql_cluster_engine_version" {
  type    = string
  default = "8"
}

variable "mongodb_cluster_instance_size" {
  type    = string
  default = "db-s-1vcpu-1gb"
}
variable "mongodb_cluster_node_count" {
  type    = number
  default = 3
}
variable "mongodb_cluster_engine_version" {
  type    = string
  default = "4"
}

variable "container_registry_server" { default = "registry.gitlab.com" }
variable "dependency_proxy_server" { default = "gitlab.com" }
variable "gitlab_group_deploy_token_username" { type = string }
variable "gitlab_group_deploy_token_password" { type = string }
variable "gitlab_cluster_agent_token" {
  type        = string
  description = "Token retrieved for Gitlab cluster agent"
}

variable "tutor_instances" { type = list(string) }

variable "vpc_ip_range" { type = string }

variable "droplet_default_image" {
  type    = string
  default = "ubuntu-20-04-x64"
}

variable "alert_manager_config" {
  type        = string
  description = "Alert Manager configuration as a YAML-encoded string"
  default     = "{}"
  validation {
    condition     = can(yamldecode(var.alert_manager_config))
    error_message = "The alert_manager_config value must be a valid YAML-encoded string."
  }
}

variable "enable_monitoring_ingress" {
  type        = bool
  default     = false
  description = "Whether to enable ingress for monitoring services."
}

variable "enable_openfaas" {
  type        = bool
  default     = false
  description = "Whether to enable OpenFAAS."
}

variable "cluster_domain" {
  type        = string
  default     = "grove.local"
  description = "Domain used as the base for monitoring services."
}

variable "lets_encrypt_notification_inbox" {
  type        = string
  default     = "contact@example.com"
  description = "Email to send any email notifications about Letsencrypt"
}
