variable "cluster_name" { type = string }
variable "cluster_max_node_count" { type = number }
variable "do_region" { type = string }
variable "vpc_uuid" { type = string }
variable "vpc_ip_range" { type = string }
