# A dockerfile to build a container that has aws-cli, kubectl, and tutor

FROM docker:20.10

ARG TERRAFORM_VERSION
ARG GITLAB_TERRAFORM_IMAGES_VERSION
ARG KUBECTL_VERSION

RUN apk add --no-cache --update \
    bash \
    build-base \
    ca-certificates \
    curl \
    git \
    jq \
    rsync \
    python3 \
    py3-pip \
    bind-tools \
    findutils \
    openssl

RUN ( curl -sLo terraform.zip "https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip" && \
      unzip terraform.zip && \
      rm terraform.zip && \
      mv ./terraform /usr/local/bin/terraform \
    ) && terraform --version

RUN curl -L "https://gitlab.com/gitlab-org/terraform-images/-/raw/${GITLAB_TERRAFORM_IMAGES_VERSION}/src/bin/gitlab-terraform.sh" -o /usr/local/bin/gitlab-terraform \
    && chmod +x /usr/local/bin/gitlab-terraform

RUN curl -L "https://storage.googleapis.com/kubernetes-release/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl" -o /usr/local/bin/kubectl \
    && chmod +x /usr/local/bin/kubectl

RUN curl -o aws-iam-authenticator https://amazon-eks.s3.us-west-2.amazonaws.com/1.19.6/2021-01-05/bin/linux/amd64/aws-iam-authenticator \
    && chmod +x ./aws-iam-authenticator \
    && mv aws-iam-authenticator /usr/local/bin/aws-iam-authenticator \
    && aws-iam-authenticator help

COPY scripts/grove.sh /usr/local/bin/grove
RUN chmod +x /usr/local/bin/grove

COPY requirements.txt requirements.txt

RUN pip3 install -r requirements.txt

CMD bash
