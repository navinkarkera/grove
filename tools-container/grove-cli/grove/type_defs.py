from typing import Dict, List, TypedDict


class InstanceCustomize(TypedDict):
    tutor: dict
    grove: dict
    env: dict


class SCSSOverride(TypedDict):
    value: str
    variable: str


class ThemeStaticFiles(TypedDict):
    dest: str
    url: str


class ImageBuildConfig(TypedDict):
    DOCKER_IMAGE_VAR: str


class GroveConfig(TypedDict):
    COMPREHENSIVE_THEME_NAME: str
    COMPREHENSIVE_THEME_SOURCE_REPO: str
    COMPREHENSIVE_THEME_VERSION: str
    ENABLE_SIMPLE_THEME_SCSS_OVERRIDES: bool
    SIMPLE_THEME_SCSS_OVERRIDES: List[SCSSOverride]
    SIMPLE_THEME_STATIC_FILES_URLS: List[ThemeStaticFiles]
    IMAGE_BUILD_CONFIG: Dict[str, ImageBuildConfig]


class TutorEnv(TypedDict):
    TUTOR_ID: str
    TUTOR_ROOT: str
    TUTOR_PLUGINS_ROOT: str
    TUTOR_K8S_NAMESPACE: str
    TUTOR_MYSQL_ROOT_PASSWORD: str

    TUTOR_MYSQL_HOST: str
    TUTOR_MYSQL_PORT: str
    TUTOR_MYSQL_ROOT_USERNAME: str
    TUTOR_MYSQL_ROOT_PASSWORD: str
    TUTOR_OPENEDX_MYSQL_USERNAME: str
    TUTOR_OPENEDX_MYSQL_DATABASE: str

    TUTOR_REDIS_HOST: str
    TUTOR_REDIS_PORT: str
    TUTOR_REDIS_USERNAME: str
    TUTOR_REDIS_PASSWORD: str

    TUTOR_GROVE_OVERRIDE_GLOBAL_PREFIX: str
    TUTOR_MONGODB_HOST: str
    TUTOR_MONGODB_DATABASE: str
    TUTOR_MONGODB_USERNAME: str
    TUTOR_MONGODB_PASSWORD: str
    TUTOR_FORUM_MONGODB_DATABASE: str
    TUTOR_GROVEOVERRIDE_MONGODB_REPLICA_SET: str

    TUTOR_OPENEDX_AWS_ACCESS_KEY: str
    TUTOR_OPENEDX_AWS_SECRET_ACCESS_KEY: str
    TUTOR_S3_HOST: str
    TUTOR_S3_STORAGE_BUCKET: str
    TUTOR_S3_REGION: str
