# -*- coding: utf-8 -*-
# Copied from https://github.com/open-craft/opencraft/blob/master/instance/newrelic.py
# OpenCraft -- tools to aid developing and hosting free software projects
# Copyright (C) 2015-2019 OpenCraft <contact@opencraft.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
New Relic API - Helper functions
"""
import logging
import os

import requests

logger = logging.getLogger(__name__)
NEWRELIC_NRQL_ALERT_CONDITION_THRESHOLD = os.environ.get("NEWRELIC_NRQL_ALERT_CONDITION_THRESHOLD", "1")
NEWRELIC_NRQL_ALERT_SIGNAL_EXPIRATION = os.environ.get("NEWRELIC_NRQL_ALERT_SIGNAL_EXPIRATION", "660")
NEWRELIC_NRQL_ALERT_CONDITION_DURATION = os.environ.get("NEWRELIC_NRQL_ALERT_CONDITION_DURATION", "11")
NEW_RELIC_REGION_US = "us"
NEW_RELIC_REGION_EU = "eu"


class NewRelicApiUrls:
    """
    Class to encapsulate new relic api logic and to assist with testing.
    """

    def __init__(self, region):
        if region == NEW_RELIC_REGION_EU:
            self.synthetics_api_url = "https://synthetics.eu.newrelic.com/synthetics/api/v3"
            self.new_relic_api_base = "https://api.eu.newrelic.com/v2"
        else:
            self.synthetics_api_url = "https://synthetics.newrelic.com/synthetics/api/v3"
            self.new_relic_api_base = "https://api.newrelic.com/v2"
        self.alerts_channels_api_url = f"{self.new_relic_api_base}/alerts_channels"
        self.alerts_policies_api_url = f"{self.new_relic_api_base}/alerts_policies"
        self.alerts_policies_channels_api_url = f"{self.new_relic_api_base}/alerts_policy_channels.json"
        self.alerts_nrql_conditions_api_url = f"{self.new_relic_api_base}/alerts_nrql_conditions"


class NewRelicApi:
    def __init__(self, api_key, region):
        self.api_urls = NewRelicApiUrls(region)
        self.newrelic_admin_user_api_key = api_key

        assert region in {NEW_RELIC_REGION_EU, NEW_RELIC_REGION_US}, "Invalid New Relic region supplied"

    def get_synthetics_monitors(self):
        """
        Return details of an active Synthetics monitor.

        Example return value:
            {
                "id": UUID,
                "name": string,
                "type": string,
                "frequency": integer,
                "uri": string,
                "locations": array of strings,
                "status": string,
                "slaThreshold": double,
                "userId": integer,
                "apiVersion": string
            }
        """
        url = f"{self.api_urls.synthetics_api_url}/monitors/"
        logger.info("GET %s", url)
        r = requests.get(url, headers=self._get_base_request_headers())
        r.raise_for_status()
        return r.json()

    def get_synthetics_monitor(self, monitor_id):
        """
        Return details of an active Synthetics monitor.

        Example return value:
            {
                "id": UUID,
                "name": string,
                "type": string,
                "frequency": integer,
                "uri": string,
                "locations": array of strings,
                "status": string,
                "slaThreshold": double,
                "userId": integer,
                "apiVersion": string
            }
        """
        url = f"{self.api_urls.synthetics_api_url}/monitors/{monitor_id}"
        logger.info("GET %s", url)
        r = requests.get(url, headers=self._get_base_request_headers())
        r.raise_for_status()
        return r.json()

    def create_synthetics_monitor(self, name, uri, monitor_type="SIMPLE", frequency=5, locations=("AWS_US_EAST_1",)):
        """
        Create a monitor for the given uri and return its id.
        """
        url = f"{self.api_urls.synthetics_api_url}/monitors"
        logger.info("POST %s %s", name, uri)
        r = requests.post(
            url,
            headers=self._get_base_request_headers(),
            json={
                "name": name,
                "uri": uri,
                "type": monitor_type,
                "frequency": frequency,
                "locations": locations,
                "status": "ENABLED",
                "options": {"verifySSL": True},  # SSL valid only for SIMPLE and BROWSER
            },
        )
        r.raise_for_status()
        return r.headers["location"].rsplit("/", 1)[-1]

    def delete_synthetics_monitor(self, monitor_id):
        """
        Delete the Synthetics monitor with the given id.

        If the monitor can't be found (DELETE request comes back with 404),
        treat it as if it has already been deleted; do not raise an exception in that case.
        """
        url = f"{self.api_urls.synthetics_api_url}/monitors/{monitor_id}"
        logger.info("DELETE %s", url)
        try:
            r = requests.delete(url, headers=self._get_base_request_headers())
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            if r.status_code == requests.codes.not_found:
                logger.info("Monitor for %s has already been deleted. Proceeding.")
            else:
                raise

    def get_alert_policies(self, name=None):
        """
        Create an alert policy with the given name and the given incident preference and return the policy id.
        """
        url = f"{self.api_urls.alerts_policies_api_url}.json"

        if name:
            params = {"filter[name]": name}
        else:
            params = None

        logger.info("GET %s %s", url, params)
        r = requests.get(
            url,
            params=params,
            headers=self._get_base_request_headers(),
        )
        r.raise_for_status()
        return r.json()

    def create_alert_policy(self, name, incident_preference="PER_POLICY"):
        """
        Create an alert policy with the given name and the given incident preference and return the policy id.
        """
        url = f"{self.api_urls.alerts_policies_api_url}.json"
        params = {"policy": {"incident_preference": incident_preference, "name": name}}
        logger.info("POST %s %s", url, params)
        r = requests.post(url, headers=self._get_base_request_headers(), json=params)
        r.raise_for_status()
        return r.json()

    def add_email_notification_channel(self, email_addresses, policy_ids, channel_prefix):
        """
        Create an email notification channel with the given email address and return the channel id.
        """
        url = f"{self.api_urls.alerts_channels_api_url}.json"
        logger.info("POST %s", url)

        r = requests.post(
            url,
            headers=self._get_base_request_headers(),
            json={
                "policy_ids": [str(z) for z in policy_ids],
                "channel": {
                    "name": f"{channel_prefix}{email_addresses}",
                    "type": "email",
                    "configuration": {"recipients": email_addresses, "include_json_attachment": False},
                },
            },
        )
        r.raise_for_status()
        return r.json()

    def get_email_notification_channels(self):
        """
        Create an email notification channel with the given email address and return the channel id.
        """
        url = f"{self.api_urls.alerts_channels_api_url}.json"
        logger.info("GET %s", url)
        r = requests.get(url, headers=self._get_base_request_headers())
        r.raise_for_status()
        return r.json()

    def add_notification_channels_to_policy(self, policy_id, channel_ids):
        """
        Update the notification channels for the given policy id with the notification channels corresponding
        to the given channel ids.
        """
        channel_list = ",".join([str(id) for id in channel_ids])
        url = f"{self.api_urls.alerts_policies_channels_api_url}?policy_id={policy_id}&channel_ids={channel_list}"
        logger.info("PUT %s", url)

        # This API call only appends to the existing notification channels and ignores the duplicates.
        headers = self._get_base_request_headers()
        headers["Content-Type"] = "application/json"
        r = requests.put(
            url,
            headers=headers,
        )
        r.raise_for_status()
        return r

    def add_alert_nrql_condition(self, policy_id, monitor_url, name):
        """
        Add a NRQL alert condition to the alert policy with the given id for the given URL.
        """
        url = f"{self.api_urls.alerts_nrql_conditions_api_url}/policies/{policy_id}.json"
        logger.info("POST %s", url)
        query = f"SELECT count(*) FROM SyntheticCheck WHERE monitorName = '{monitor_url}' AND result = 'FAILED'"
        r = requests.post(
            url,
            headers=self._get_base_request_headers(),
            json={
                "nrql_condition": {
                    "type": "static",
                    "name": name,
                    "enabled": True,
                    "terms": [
                        {
                            "duration": NEWRELIC_NRQL_ALERT_CONDITION_DURATION,
                            "threshold": NEWRELIC_NRQL_ALERT_CONDITION_THRESHOLD,
                            "operator": "above",
                            "priority": "critical",
                            "time_function": "any",
                        }
                    ],
                    "nrql": {
                        "query": query,
                        "since_value": "3",
                    },
                    "signal": {"fill_option": "static", "fill_value": "0"},
                    "expiration": {
                        "expiration_duration": NEWRELIC_NRQL_ALERT_SIGNAL_EXPIRATION,
                        "open_violation_on_expiration": False,
                        "close_violations_on_expiration": True,
                    },
                }
            },
        )
        r.raise_for_status()
        return r.json()["nrql_condition"]["id"]

    def delete_alert_policy(self, policy_id):
        """
        Delete the New Relic alerts alert policy with the given id.
        """
        url = f"{self.api_urls.alerts_policies_api_url}/{policy_id}.json"
        logger.info("DELETE %s", url)
        try:
            r = requests.delete(url, headers=self._get_base_request_headers())
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            if r.status_code == requests.codes.not_found:
                logger.info("Alert policy for %s has already been deleted. Proceeding.")
            else:
                raise

    def delete_email_notification_channel(self, channel_id):
        """
        Delete the New Relic email notification channel with the given id.

        If the email notification channel can't be found, treat it as if it has already been deleted
        and do not raise an exception in that case.
        """
        url = f"{self.api_urls.alerts_channels_api_url}/{channel_id}.json"
        logger.info("DELETE %s", url)
        try:
            r = requests.delete(url, headers=self._get_base_request_headers())
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            if r.status_code == requests.codes.not_found:
                logger.info("Email notification channel for %s has already been deleted. Proceeding.")
            else:
                raise

    def remove_policy_from_channel(self, policy_id, channel_id):
        """
        Removes the provided policy_id from the alerts channel_id
        """
        url = self.api_urls.alerts_policies_channels_api_url, channel_id

        params = {"channel_id": channel_id, "policy_id": policy_id}
        logger.info("DELETE %s %s", url, params)

        try:
            r = requests.delete(url, params=params, headers=self._get_base_request_headers())
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            if r.status_code == requests.codes.not_found:
                logger.info("Email notification channel for %s has already been deleted. Proceeding.")
            else:
                raise

    def delete_alert_nrql_condition(self, condition_id):
        """
        Delete the New Relic NRQL alert condition with the given condition_id.

        If the alert condition can't be found (DELETE request comes back with 404),
        treat it as if it has already been deleted; do not raise an exception in that case.
        """
        url = f"{self.api_urls.alerts_nrql_conditions_api_url}/{condition_id}.json"
        logger.info("DELETE %s", url)
        try:
            r = requests.delete(url, headers=self._get_base_request_headers())
            r.raise_for_status()
        except requests.exceptions.HTTPError:
            if r.status_code == requests.codes.not_found:
                logger.info("Alert condition for %s has already been deleted. Proceeding.")
            else:
                raise

    def get_alert_nrql_conditions(self, policy_id):
        """
        Delete the New Relic NRQL alert condition with the given condition_id.

        If the alert condition can't be found (DELETE request comes back with 404),
        treat it as if it has already been deleted; do not raise an exception in that case.
        """
        url = f"{self.api_urls.alerts_nrql_conditions_api_url}.json"
        params = {"policy_id": policy_id}
        logger.info("GET %s", url, params)

        try:
            r = requests.get(url, params=params, headers=self._get_base_request_headers())
            r.raise_for_status()
            return r.json()
        except requests.exceptions.HTTPError:
            if r.status_code == requests.codes.not_found:
                logger.info("Alert condition for %s has already been deleted. Proceeding.")
            else:
                raise

    def _get_base_request_headers(self):
        """
        Request headers for the New Relic API.
        """
        return {"X-Api-Key": self.newrelic_admin_user_api_key, "Content-Type": "application/json"}
