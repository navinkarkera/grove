import json
from pathlib import Path
from unittest.mock import patch

import pytest

from grove.const import INSTANCES_DIR
from grove.exceptions import GroveError
from grove.instance import Instance
from grove.utils import slugify_instance_name

from .utils import TEST_INSTANCE_NAME_A, GroveTestCase


class InstanceTestCase(GroveTestCase):
    """
    Test grove.instance.Instance
    """

    def test_instance_init(self):
        """
        Test if Instance initializes correctly.
        """
        test_instance_name = TEST_INSTANCE_NAME_A
        instance = Instance(test_instance_name)

        assert slugify_instance_name(test_instance_name) == instance.instance_name
        assert INSTANCES_DIR / instance.instance_name == instance.root_dir
        assert INSTANCES_DIR / instance.instance_name / "plugins" == instance.plugin_dir
        assert INSTANCES_DIR / instance.instance_name / "env" == instance.env_dir
        assert INSTANCES_DIR / instance.instance_name / "env-overrides" == instance.env_override_dir
        assert INSTANCES_DIR / instance.instance_name / "grove.yml" == instance.grove_config_file
        assert INSTANCES_DIR / instance.instance_name / "config.yml" == instance.tutor_config_file
        assert instance.plugin_dir / "groveoverride.yml" == instance.plugin_file

    def test_instance_create(self):
        instance = Instance(TEST_INSTANCE_NAME_A)

        assert instance.exists() is False

        instance.create()

        assert instance.exists()

        assert Path.exists(instance.root_dir)
        assert Path.exists(instance.plugin_dir)
        assert Path.exists(instance.grove_config_file)
        assert Path.exists(instance.tutor_config_file)

        instance.load_instance_configs()

        assert "COMPREHENSIVE_THEME_SOURCE_REPO" in instance.grove_config.keys()
        assert "PLUGINS" in instance.tutor_config.keys()

        instance.delete()
        assert not Path.exists(instance.root_dir)

    def test_instance_double_create_error(self):
        instance = Instance(TEST_INSTANCE_NAME_A)
        instance.create()

        with pytest.raises(GroveError) as exc:
            instance.create()
            assert str(exc) == f"Instance {TEST_INSTANCE_NAME_A} already exists. Can't create."

        assert instance.exists() is True

    def test_instance_create_error_invalid_name(self):
        instance = Instance("monitoring")

        with pytest.raises(GroveError) as exc:
            instance.create()
            assert str(exc) == "Instance name \"monitoring\" is not valid. Can't create."

        assert instance.exists() is False

    def test_instance_prepare(self):
        test_instance_name = TEST_INSTANCE_NAME_A
        instance = Instance(test_instance_name)
        instance.prepare(
            {
                "grove": {
                    "SIMPLE_THEME_SCSS_OVERRIDES": [
                        {
                            "variable": "$bg-color",
                            "value": "#000000",
                        },
                    ]
                },
                "tutor": {
                    "MFE_HOST": "app.test.com",
                },
                "env": {
                   "LMS": {
                      "MKTG_URL_OVERRIDES": {
                         "ABOUT": "https://opencraft.com/about-us/"
                      }
                   }
                }
            }
        )

        self.mock_tf.is_infrastructure_ready.assert_called_with(instance.instance_name)
        self.mock_tf.get_env.assert_called_with(instance.instance_name)

        assert "COMPREHENSIVE_THEME_SOURCE_REPO" in instance.grove_config.keys()
        assert "SIMPLE_THEME_SCSS_OVERRIDES" in instance.grove_config.keys()

        assert len(instance.grove_config["SIMPLE_THEME_SCSS_OVERRIDES"]) == 1
        assert instance.grove_config["SIMPLE_THEME_SCSS_OVERRIDES"][0]["variable"] == "$bg-color"
        assert instance.grove_config["SIMPLE_THEME_SCSS_OVERRIDES"][0]["value"] == "#000000"

        assert "ID" in instance.tutor_config.keys()
        assert "JWT_RSA_PRIVATE_KEY" in instance.tutor_config.keys()
        assert instance.tutor_config["MFE_HOST"] == "app.test.com"

        lms_env_config = json.loads(f"{{{instance.plugin_config[instance.PATCH_KEY]['lms-env']}}}")
        assert lms_env_config['MKTG_URL_OVERRIDES']['ABOUT'] == "https://opencraft.com/about-us/"

        instance.delete()
        assert not Path.exists(instance.root_dir)

    @patch("grove.instance.os")
    def test_instance_name_validity(self, mock_os):
        scenarios = (
            # Instance names
            ("testinstance", "testinstance", [], True),
            ("test-instance", "test-instance", [], True),
            ("test_instance", "test-instance", [], True),
            ("test instance", "test-instance", [], True),

            # Instance already registered
            ("testinstance", "testinstance", ["testinstance"], False),

            # Restricted instance names (because of namespace name)
            ("default", "default", [], False),
            ("gitlab-kubernetes-agent", "gitlab-kubernetes-agent", [], False),
            ("kube-node-lease", "kube-node-lease", [], False),
            ("kube-public", "kube-public", [], False),
            ("kube-system", "kube-system", [], False),
            ("monitoring", "monitoring", [], False),
            ("openfaas", "openfaas", [], False),
            ("openfaas-fn", "openfaas-fn", [], False),

            # Illegal instance name
            ("1testinstance", "1testinstance", [], False),

            # Instance name length issues
            ("a", "a", [], False),
            ("ab", "ab", [], False),
            (
                "this instance name is soooooooooooooooooooooo long",
                "this-instance-name-is-soooooooooooooooooooooo-long",
                [],
                False,
            ),
        )

        for scenario in scenarios:
            instance_name, instance_slug, registered, expected = scenario

            mock_os.listdir.return_value = registered
            mock_os.is_dir.return_value = True

            instance = Instance(instance_name)

            assert instance.instance_name == instance_slug, f"Scenario {scenario}"
            assert instance.is_name_valid() == expected, f"Scenario {scenario}"
