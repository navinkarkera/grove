#!/usr/bin/env bash
# This is a wrapper for gitlab-terraform


set -e
set -u

WORKSPACE=${CI_PROJECT_DIR:-/workspace}

# get space seperated string of instances
cd $WORKSPACE/instances/
instance_dirs=$(find * -maxdepth 0 -printf "\"%p\", ")

# export required instances list variable for terraform
export TF_VAR_tutor_instances="[${instance_dirs::-2}]"

# move to TF_ROOT and use gitlab-terraform
cd $TF_ROOT && gitlab-terraform "$@"
