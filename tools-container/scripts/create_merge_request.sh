#!/usr/bin/env bash
set -euo pipefail

WORKSPACE="${CI_PROJECT_DIR:-/workspace}"
COMMIT_MESSAGE_PREFIX="[AutoDeploy]"

# Ensure that the given variable is not empty, otherwise print error and exit.
function ensure_not_empty() {
  local value=$(eval "echo \"\${${1}}\"")
  [[ -z "${value}" ]] && { echo "Please provide \"${1}\" variable." >&2 && exit 1; } || true
}

# Ensure that the given variable contains no whitespaces, otherwise print error and exit.
function ensure_no_whitespaces() {
  local value=$(eval "echo \"\${${1}}\"")
  [[ "${value}" == *" "* ]] && { echo "\"${1}\" variables cannot contain spaces." >&2 && exit 1; } || true
}

# Validate that instance-related variables are set as expected.
function validate_instance_vars() {
  ensure_not_empty "INSTANCE_NAME"
  ensure_no_whitespaces "INSTANCE_NAME"
}

# Validate that deployment-related variables are set as expected.
function validate_deployment_vars() {
  ensure_not_empty "DEPLOYMENT_REQUEST_ID"
  ensure_no_whitespaces "DEPLOYMENT_REQUEST_ID"
}

# Configure the git client and set the remote branch for being able to push changes.
function configure_git() {
  echo "Configuring Git..."

  ensure_not_empty "CI_PROJECT_PATH"
  ensure_not_empty "CI_SERVER_HOST"
  ensure_not_empty "GITLAB_TOKEN"
  ensure_not_empty "GITLAB_USER_EMAIL"
  ensure_not_empty "GITLAB_USER_ID"

  # Deploy tokens don't support pushing to the repository: https://gitlab.com/gitlab-org/gitlab/-/issues/223679
  git remote set-url origin "https://${GITLAB_USER_ID}:${GITLAB_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}"
  git config --global user.email "$GITLAB_USER_EMAIL"
  git config --global user.name "$GITLAB_USER_EMAIL"
}

# Push instance changes to git remote. Requires pre-configured git client.
function push_instance_changes() {
  local branch="${1}"
  local commit_message="${2}"

  echo "Pushing changes to ${branch}"
  git commit --allow-empty -am "${commit_message}"
  git push \
    -o merge_request.create \
    -o merge_request.target="${CI_COMMIT_REF_NAME}" \
    -o merge_request.merge_when_pipeline_succeeds \
    -o merge_request.remove_source_branch \
    -o merge_request.title="${commit_message}" \
    origin "${branch}"
}

# Handle instance creation or update.
function create_or_update_instance() {
  export COMMIT_TYPE="${1}"

  ensure_not_empty "COMMIT_TYPE"
  ensure_no_whitespaces "COMMIT_TYPE"

  validate_instance_vars
  validate_deployment_vars
  configure_git

  local commit_message="${COMMIT_MESSAGE_PREFIX}[${COMMIT_TYPE}] ${INSTANCE_NAME}|${DEPLOYMENT_REQUEST_ID}"
  local branch="deployment/${INSTANCE_NAME}/${DEPLOYMENT_REQUEST_ID}"

  git checkout -b "${branch}"

  echo "Preparing deployment for ${INSTANCE_NAME}"
  grove prepare "${INSTANCE_NAME}"
  git add "instances/${INSTANCE_NAME}"

  push_instance_changes "${branch}" "${commit_message}"
}

# Archive the given instance
function archive_instance() {
  validate_instance_vars
  configure_git

  local commit_message="${COMMIT_MESSAGE_PREFIX}[Archive] ${INSTANCE_NAME}"
  local branch="archive/${INSTANCE_NAME}"

  git checkout -b "${branch}"

  echo "Removing instance"
  git rm -r "instances/${INSTANCE_NAME}"

  push_instance_changes "${branch}" "${commit_message}"
}

# Main function; serves as an entrypoint.
function main() {
  [[ ! -d "${WORKSPACE}" ]] && { echo "${WORKSPACE} is not a directory" && exit 1; }

  # Create a new instance or update an existing one
  if [ -n "${NEW_INSTANCE_TRIGGER:-}" ]; then
    local instance_operation=""

    if [[ -d "$WORKSPACE/instances/$INSTANCE_NAME" ]]; then
      instance_operation="Update"
    else
      instance_operation="Create"
    fi

    echo "${instance_operation} ${INSTANCE_NAME}"
    create_or_update_instance "${instance_operation}"

  # Archive an existing instance
  elif [ -n "${ARCHIVE_INSTANCE_TRIGGER:-}" ]; then
    echo "Archive ${INSTANCE_NAME}"
    archive_instance

  # No trigger set, abort
  else
    echo "Operation not recognized." >&2
    exit 1
  fi
}

main
