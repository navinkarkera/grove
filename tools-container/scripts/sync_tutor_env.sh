#!/usr/bin/env bash

set -e

CURRENT_DIR=${BASH_SOURCE%/*}

if [[ -z "${TF_VAR_cluster_provider}" ]]; then
    echo "Please set the 'TF_VAR_cluster_provider' env variable. Options are 'aws' and 'digitalocean'"
    exit 1
fi

if [[ -z "${TUTOR_ROOT}" ]]; then
    echo "Please set the 'TUTOR_ROOT' env variable with the full path to the tutor project"
    exit 1
fi

if [[ -z "${TUTOR_ID}" ]]; then
    echo "Please set the 'TUTOR_ID' env variable name of the tutor project"
    exit 1
fi

EXTRA_ARGS=""

if [ "${TF_VAR_cluster_provider}" == "digitalocean" ]; then

    if [[ -z "${TF_VAR_do_region}" ]]; then
        echo "Please set the 'TF_VAR_do_region' env variable"
        exit 1
    fi

    export AWS_ACCESS_KEY_ID=$SPACES_ACCESS_KEY_ID
    export AWS_SECRET_ACCESS_KEY=$SPACES_SECRET_ACCESS_KEY
    EXTRA_ARGS="--endpoint=https://${TF_VAR_do_region}.digitaloceanspaces.com"
fi

# get s3 bucket name for tutor env folder
ENV_BUCKET_NAME=`bash $CURRENT_DIR/tf.sh output -json tutor_env_bucket | tr -d '"'`

ENV_DIRECTORY=${TUTOR_ROOT}/env

if [ $1 == "pull" ]; then
    # download env directory from s3 if exists
    aws s3 sync s3://${ENV_BUCKET_NAME}/${TUTOR_ID}/env $ENV_DIRECTORY ${EXTRA_ARGS}
elif [ $1 == "push" ]; then
    # upload updated env directory to s3
    aws s3 sync $ENV_DIRECTORY s3://${ENV_BUCKET_NAME}/${TUTOR_ID}/env ${EXTRA_ARGS} --delete
else
    echo "Invalid operation!"
fi
