variable "aws_region" {
  type        = string
  default     = "us-east-1"
  description = "AWS region."
}

variable "environment" {
  type        = string
  description = "A name that identifies the environment, will used as prefix and for tagging."
}

variable "runner_name" {
  type        = string
  default     = "grove-gitlab-runner"
  description = "Name of the runner, will be used in the runner config.toml"
}

variable "runners_concurrent" {
  type        = number
  default     = 30
  description = "Concurrent value for the runners, will be used in the runner config.toml."
}

variable "runners_idle_count" {
  type        = number
  default     = 0
  description = "Idle count of the runners, will be used in the runner config.toml."
}

variable "instance_type" {
  type        = string
  default     = "t3.small"
  description = "Instance type used for the GitLab runner."
}

variable "gitlab_url" {
  type        = string
  default     = "https://gitlab.com"
  description = "URL of the gitlab instance to connect to."
}

variable "registration_token" {
  type        = string
  description = "GitLab token used for registering CI runners."
  sensitive   = true
}
